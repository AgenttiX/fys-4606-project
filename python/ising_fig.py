import matplotlib.pyplot as plt
import numpy as np

import utils

fig: plt.Figure = plt.figure()
ax: plt.Axes = fig.add_subplot()

arr = np.random.randint(0, 2, (5, 5))
ax.imshow(arr, cmap="Greys", vmin=0, vmax=2)
ax.set_xticks(np.arange(arr.shape[1]+1)-.5, minor=True)
ax.set_yticks(np.arange(arr.shape[0]+1)-.5, minor=True)
ax.tick_params(axis="both", which="both", length=0, labelsize=0)
ax.grid(which="minor")

for inds, val in np.ndenumerate(arr):
    ax.text(inds[1], inds[0], "↓" if val else "↑", ha="center", va="center", size="xx-large")

utils.savefig(fig, "./fig/ising")
plt.show()
