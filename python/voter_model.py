import typing as tp

import numba
import numpy as np

import utils


class VoterModel:
    def __init__(self, shape: tp.Tuple[int, ...], rho0: float):
        lattice_randvals = utils.random_arr(shape)
        self._lattice: np.ndarray = ((lattice_randvals < rho0).astype(np.int_) * 2 - 1).astype(np.int8)
        self._neighbor_inds = self.gen_neighbor_inds()

    def copy_from_neighbor(self):
        """Choose a random voter and spread its opinion to a random neighbor

        Using tuples as indices was not jittable, so this method is overridden in the subclasses
        """
        voter = self.random_voter()
        # neighbors = self.neighbor_inds(voter)
        neighbors = self._neighbor_inds[voter]
        random_neighbor = neighbors[np.random.randint(0, len(neighbors)), :]
        self._lattice[tuple(voter)] = self._lattice[tuple(random_neighbor)]

    def gen_neighbor_inds(self):
        arr = np.empty(self._lattice.shape + (2*self._lattice.ndim, self._lattice.ndim), dtype=np.int_)
        for loc, _ in np.ndenumerate(self._lattice):
            neigh = self.neighbor_inds(list(loc))
            arr[loc] = neigh
        return arr

    def has_consensus(self) -> int:
        sum = self._lattice.sum()
        if sum == self._lattice.size:
            return 1
        elif sum == -self._lattice.size:
            return -1
        return 0

    def neighbor_inds(self, loc: tp.Union[tp.Tuple[int, ...], tp.List[int]]) -> np.ndarray:
        # This does not work with Numba
        # inds = []
        # for i in range(self.__lattice.ndim):
        #     inds.append((*loc[:i], (loc[i]+1) % self.__lattice.shape[i], *loc[i+1:]))
        #     inds.append((*loc[:i], (loc[i]-1) % self.__lattice.shape[i], *loc[i+1:]))
        inds = np.empty((2 * self._lattice.ndim, self._lattice.ndim), dtype=np.int_)
        for i in range(self._lattice.ndim):
            inds[2*i, :i] = loc[:i]
            inds[2*i, i] = (loc[i] + 1) % self._lattice.shape[i]
            inds[2*i, i+1:] = loc[i+1:]
            inds[2*i+1, :i] = loc[:i]
            inds[2*i+1, i] = (loc[i] - 1) % self._lattice.shape[i]
            inds[2*i+1, i+1:] = loc[i+1:]
        return inds

    def prob_of_plus(self) -> float:
        """rho"""
        return np.sum(self._lattice > 0) / self._lattice.size

    def random_voter(self) -> tp.List[int]:
        return [np.random.randint(0, size) for size in self._lattice.shape]


@numba.jitclass([
    ("_lattice", numba.int8[:]),
    ("_neighbor_inds", numba.int_[:, :, :])
])
class VoterModel1D(VoterModel):
    def copy_from_neighbor(self):
        voter = self.random_voter()
        neighbors = self._neighbor_inds[voter[0]]
        neighbor = neighbors[np.random.randint(0, len(neighbors)), :]
        self._lattice[voter[0]] = self._lattice[neighbor[0]]


@numba.jitclass([
    ("_lattice", numba.int8[:, :]),
    ("_neighbor_inds", numba.int_[:, :, :, :])
])
class VoterModel2D(VoterModel):
    def copy_from_neighbor(self):
        voter = self.random_voter()
        neighbors = self._neighbor_inds[voter[0], voter[1]]
        neighbor = neighbors[np.random.randint(0, len(neighbors)), :]
        self._lattice[voter[0], voter[1]] = self._lattice[neighbor[0], neighbor[1]]


@numba.jitclass([
    ("_lattice", numba.int8[:, :, :]),
    ("_neighbor_inds", numba.int_[:, :, :, :, :])
])
class VoterModel3D(VoterModel):
    def copy_from_neighbor(self):
        voter = self.random_voter()
        neighbors = self._neighbor_inds[voter[0], voter[1], voter[2]]
        neighbor = neighbors[np.random.randint(0, len(neighbors)), :]
        self._lattice[voter[0], voter[1], voter[2]] = self._lattice[neighbor[0], neighbor[1], neighbor[1]]


def test():
    mod1 = VoterModel1D((10,), 0.5)
    mod2 = VoterModel2D((10, 10), 0.5)
    mod3 = VoterModel3D((10, 10, 10), 0.5)
    for mod in (mod1, mod2, mod3):
        mod.random_voter()
        mod.copy_from_neighbor()
        mod.has_consensus()
        mod.prob_of_plus()


if __name__ == "__main__":
    test()
