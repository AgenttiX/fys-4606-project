import gc
import threading
import time
import typing as tp

import matplotlib
import matplotlib.pyplot as plt
import numba
import numba.typed
import numpy as np
import scipy.optimize

import part_c
import sznajd_model
import utils
import video

matplotlib.use("tkAgg")


@numba.njit(parallel=True, cache=True)
def run_until_converged(L: int, fractions: np.ndarray, runs: int, iters_per_check: int, max_checks: int):
    arr_iters = np.zeros((runs, ), dtype=np.int_)
    arr_winners = np.zeros((runs, ), dtype=np.int_)
    for i_model in numba.prange(runs):
        model = sznajd_model.SznajdModel(L, fractions)
        iters, winner = model.run_until_converged(iters_per_check, max_checks)
        arr_iters[i_model] = iters
        arr_winners[i_model] = winner
    return arr_iters, arr_winners


def fit_linear(x, a, b):
    return a*x + b


def part_d():
    n_opinions = np.arange(2, 11)
    fractions = [[1/i]*i for i in n_opinions]
    n_runs = 64
    L = 50
    iters_per_check = 1000
    max_checks = 100000

    arr_times = np.empty((len(fractions), ))
    arr_iters = np.empty((len(fractions), n_runs), dtype=np.int_)
    arr_winners = np.empty_like(arr_iters)

    for i_frac in range(len(fractions)):
        start_time = time.perf_counter()
        frac = np.array(fractions[i_frac])
        iters, winners = run_until_converged(L, frac, n_runs, iters_per_check, max_checks)
        end_time = time.perf_counter()
        print("Results for n_opinions =", n_opinions[i_frac])
        print(frac)
        print(iters)
        print(winners)
        print(f"{end_time - start_time:.2f} s")
        print()
        arr_times[i_frac] = end_time - start_time
        arr_iters[i_frac, :] = iters
        arr_winners[i_frac, :] = winners

    arr_iters_mean = arr_iters.mean(axis=1)
    arr_winners_mean = arr_winners.mean(axis=1)
    fit_iters = scipy.optimize.curve_fit(fit_linear, n_opinions, arr_iters_mean)
    fit_winners = scipy.optimize.curve_fit(fit_linear, n_opinions, arr_winners_mean)

    # fig: plt.Figure = plt.figure(figsize=utils.DISPLAY_SIZE)
    fig: plt.Figure = plt.figure(figsize=utils.A4_SIZE)
    # fig.suptitle("Sznajd model with multiple opinions")
    ax1: plt.Axes = fig.add_subplot(121)
    ax2: plt.Axes = fig.add_subplot(122)

    ax1.errorbar(n_opinions, arr_iters_mean, yerr=arr_iters.std(axis=1), label="data", marker="*", linestyle="")
    ax1.plot(n_opinions, fit_linear(n_opinions, *fit_iters[0]), label=f"{fit_iters[0][0]:.2f}n + {fit_iters[0][1]:.2f}")
    ax1.set_xlabel("Number of different opinions")
    ax1.set_ylabel("Iterations before convergence")
    ax1.legend()

    ax2.plot(n_opinions, arr_winners_mean, label="data", marker="*", linestyle="")
    ax2.plot(n_opinions, fit_linear(n_opinions, *fit_winners[0]), label=f"{fit_winners[0][0]:.2f}n - {abs(fit_winners[0][1]):.2f}")
    ax2.set_xlabel("Number of different opinions")
    ax2.set_ylabel("Average consensus of all runs")
    ax2.legend()

    # utils.savefig(fig, "./fig/d")
    utils.savefig(fig, "./fig/d_report")
    plt.show()


def make_video():
    L = 500
    frame_shape = (L, L)
    batches = 5
    iters_per_frame = 200000
    frames_per_batch = 500
    frames = frames_per_batch*batches
    make_video = True
    n_models = 1
    print("Video length:", frames / 60, "s")

    models: tp.Union[numba.typed.List, tp.List[sznajd_model.SznajdModel]] = numba.typed.List()
    models.append(sznajd_model.SznajdModel(L, np.array([1/3, 1/3, 1/3])))
    arr = np.empty((frames_per_batch, *frame_shape))
    arr_mean = np.empty((n_models, frames))
    vid = video.Video(frame_shape, path="./video/d.mp4", temp_path="./video/d_temp.mp4")
    for i_batch in range(batches):
        start_time = time.perf_counter()
        part_c.part_c_core(arr, arr_mean, models, i_batch, iters_per_frame, frames_per_batch)
        compute_ready = time.perf_counter()
        if make_video:
            for i_frame in range(frames_per_batch):
                vid.tricolor_frame(arr[i_frame, :, :])
        print(
            f"Batch: {i_batch}, frames: {i_batch*frames_per_batch} / {frames}, "
            f"compute: {compute_ready - start_time :.2f}, video: {time.perf_counter() - compute_ready:.2f}")

    # Free memory for video conversion
    del arr
    gc.collect()

    if make_video:
        thr = threading.Thread(target=vid.process)
        thr.start()
    else:
        thr = None

    if make_video:
        print("Waiting for video conversion thread")
        thr.join()
        print("Ready")


if __name__ == "__main__":
    part_d()
    # make_video()
