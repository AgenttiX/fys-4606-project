import gc
import os
import threading
import time
import typing as tp

import matplotlib
import matplotlib.pyplot as plt
import numba
import numba.typed
import numpy as np
import scipy.optimize

import sznajd_model
import utils
import video

matplotlib.use("tkAgg")


@numba.njit(parallel=True, cache=True)
def part_c_core(
        arr: np.ndarray,
        arr_mean: np.ndarray,
        models: tp.Union[numba.typed.List, tp.List[sznajd_model.SznajdModel]],
        i_batch: int, iters_per_frame: int, frames_per_batch: int):
    L = models[0].L
    for i_model in numba.prange(len(models)):
        model = models[i_model]
        for i_frame in range(frames_per_batch):
            for i_iter in range(iters_per_frame):
                model.run()
            arr[i_frame, :, i_model*L:(i_model+1)*L] = model.lattice
            arr_mean[i_model, i_batch*frames_per_batch + i_frame] = model.lattice.mean()


def plot_mean(arr_mean: np.ndarray, models: tp.List[sznajd_model.SznajdModel], iters_per_frame: int):
    # fig: plt.Figure = plt.figure(figsize=utils.DISPLAY_SIZE)
    fig: plt.Figure = plt.figure(figsize=utils.REPORT_SIZE)
    ax1: plt.Axes = fig.add_subplot(111)

    def exp_fun(x, a, b):
        return a*np.exp(b*x)

    def exp_fun2(x, a, b):
        return 1 - a*np.exp(b*x)

    arr_frames = np.arange(arr_mean.shape[1])
    arr_iters = arr_frames*iters_per_frame
    all_converged = True
    converged_to_one = False
    for i_model, model in enumerate(models):
        arr_mod_mean = arr_mean[i_model, :]
        label = ", ".join([f"{perc} %" for perc in model.fractions])
        ax1.plot(arr_iters, arr_mod_mean, label=label)

        zero_inds = np.argwhere(arr_mod_mean == 0)
        one_inds = np.argwhere(arr_mod_mean == 1)
        if len(zero_inds):
            first_zero_ind = zero_inds[0, 0]
            popt, pcov = scipy.optimize.curve_fit(exp_fun, arr_frames[:first_zero_ind], arr_mod_mean[:first_zero_ind], p0=(0.5, -1e-3))
        elif len(one_inds):
            converged_to_one = True
            first_one_ind = one_inds[0, 0]
            popt, pcov = scipy.optimize.curve_fit(exp_fun2, arr_frames[:first_one_ind], arr_mod_mean[:first_one_ind], p0=(0.5, -1e-3))
        else:
            print(f"Model with {label} did not converge")
            all_converged = False
            continue
        # print(popt)
        # print(pcov)
        ax1.plot(arr_iters, exp_fun(arr_frames, popt[0], popt[1]), label=f"{label} fit")
    # fig.suptitle("Means")
    ax1.set_xlabel("Iterations")
    ax1.set_ylabel("Mean")
    ax1.legend()

    if converged_to_one:
        filename = "_conv_to_one"
    elif not all_converged:
        filename = "_nonconverged"
    else:
        filename = ""
    # utils.savefig(fig, "./fig/c_mean" + filename)
    utils.savefig(fig, "./fig/c_mean_report" + filename)
    return filename


def part_c():
    fractions = np.array([[0.51, 0.49], [0.55, 0.45], [0.60, 0.40]], dtype=np.float_)
    n_models = fractions.shape[0]
    L = 100
    frame_shape = (L, fractions.shape[0]*L)

    batches = 10
    frames_per_batch = 10000
    frames = batches*frames_per_batch
    iters_per_frame = 100
    make_video = False

    models: tp.Union[numba.typed.List, tp.List[sznajd_model.SznajdModel]] = numba.typed.List()
    for i in range(n_models):
        models.append(sznajd_model.SznajdModel(L, fractions[i, :]))
    arr = np.empty((frames_per_batch, *frame_shape))
    arr_mean = np.empty((n_models, frames))

    fig: plt.Figure = plt.figure(figsize=utils.REPORT_FULL_PAGE)
    axes = fig.subplots(3, 3)
    plot_iters = (100, 1000, 10000)
    vid = video.Video(frame_shape, path="./video/c_conv_temp.mp4", temp_path="./video/c_temp.mp4") if make_video else None
    for i_batch in range(batches):
        start_time = time.perf_counter()
        part_c_core(arr, arr_mean, models, i_batch, iters_per_frame, frames_per_batch)
        compute_ready = time.perf_counter()
        if make_video:
            for i_frame in range(frames_per_batch):
                vid.frame(arr[i_frame, :, :])
        if i_batch == 0:
            for i_run in range(3):
                for i_iters, iters in enumerate(plot_iters):
                    axes[i_iters, i_run].imshow(arr[iters-1, :, i_run*L:(i_run+1)*L].copy())
        print(
            f"Batch: {i_batch}, frames: {i_batch*frames_per_batch} / {frames}, "
            f"compute: {compute_ready - start_time :.2f}, video: {time.perf_counter() - compute_ready:.2f}")

    for i_iters, iters in enumerate(plot_iters):
        for i_fracs, fracs in enumerate(fractions):
            ax: plt.Axes = axes[i_iters, i_fracs]
            ax.set_title("(" + ", ".join([f"{perc} %" for perc in fracs]) + f"), {iters} iters")

    # Free memory for video conversion
    del arr
    gc.collect()

    if make_video:
        thr = threading.Thread(target=vid.process)
        thr.start()
    else:
        thr = None

    filename = plot_mean(arr_mean, models, iters_per_frame)
    utils.savefig(fig, "./fig/c_evolution_report")
    plt.show()

    if make_video:
        print("Waiting for video conversion thread")
        thr.join()
        os.rename("./video/c_conv_temp.mp4", f"./video/c{filename}.mp4")
        print("Ready")


if __name__ == "__main__":
    part_c()
