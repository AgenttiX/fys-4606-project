import os
import subprocess
import typing as tp

import cv2
import numpy as np


class Video:
    def __init__(
            self, shape: tp.Tuple[int, int],    # (y, x)
            temp_path: str = "temp.mp4", path: str = "converted.mp4",
            fps: int = 60, fourcc: str = "mp4v"):
        fourcc_obj = cv2.VideoWriter_fourcc(*fourcc)
        self.__shape = shape
        self._file = cv2.VideoWriter(temp_path, fourcc_obj, fps, (self.__shape[1], self.__shape[0]))
        self.__temp_path = temp_path
        self.__path = path

    def __del__(self):
        if self._file is not None:
            self._file.release()

    def frame(self, arr: np.ndarray):
        frame = cv2.normalize(arr, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
        self._file.write(cv2.merge([frame, frame, frame]))

    def tricolor_frame(self, arr: np.ndarray):
        frame = np.zeros((3, *self.__shape), dtype=np.uint8)
        frame[0, arr == 0] = 255
        frame[1, arr == 1] = 255
        frame[2, arr == 2] = 255
        self._file.write(cv2.merge(frame))

    def colored_frame(self, r: int, g: int, b: int, num: int = 1):
        arr = np.ones((3, *self.__shape), dtype=np.uint8)
        arr[0, :, :] *= b
        arr[1, :, :] *= g
        arr[2, :, :] *= r
        for i in range(num):
            self._file.write(cv2.merge(arr))

    def process(self):
        self._file.release()
        self._file = None
        if os.path.exists(self.__path):
            print("Removing old converted file")
            os.remove(self.__path)
        subprocess.call(["ffmpeg", "-i", self.__temp_path, "-vcodec", "libx264", self.__path])
