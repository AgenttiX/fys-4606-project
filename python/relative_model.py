import numba
import numpy as np

import utils


@numba.jitclass([
    ("__voters", numba.uint8[:]),
    ("__eps", numba.uint8),
    ("__m", numba.float_),
    ("__disp_x", numba.int_),
    ("__disp_y", numba.int_)
])
class RelativeModel:
    def __init__(self, n: int, eps: int, m: int, disp_x: int, disp_y: int):
        self.__voters = utils.randint(0, 255, n, dtype=np.uint8)
        self.__eps = eps
        self.__m = m
        if disp_x*disp_y != n:
            raise ValueError("Invalid display dimensions")
        self.__disp_x = disp_x
        self.__disp_y = disp_y

    def run(self, iters: int = 1):
        for _ in range(iters):
            i = np.random.randint(self.__voters.size)
            j = np.random.randint(self.__voters.size)
            diff = self.__voters[i] - self.__voters[j]
            if abs(diff) < self.__eps:
                change = round(self.__m / 2 * diff)
                # voters[i] < voters[j] => diff < 0 => -= change increases voters[i] => OK
                self.__voters[i] -= change
                self.__voters[j] += change

    def get_grid(self) -> np.ndarray:
        return self.__voters.copy().reshape((self.__disp_y, self.__disp_x))
