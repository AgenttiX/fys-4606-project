import gc
import threading
import time
import typing as tp

import matplotlib
import matplotlib.colors
import matplotlib.pyplot as plt
import numba
import numba.typed
import numpy as np

import relative_model
import utils
import video

matplotlib.use("tkAgg")


@numba.njit(parallel=True, cache=True)
def part_b_core(
        arr: np.ndarray,
        models: tp.Union[numba.typed.List, tp.List[relative_model.RelativeModel]],
        arr_eps: np.ndarray, arr_m: np.ndarray,
        arr_mean: np.ndarray, arr_std: np.ndarray,
        disp_x: int, disp_y: int, frames: int, iters_per_frame: int, batch_ind: int):
    for i_model in numba.prange(arr_eps.size*arr_m.size):
        i_eps = i_model % arr_eps.size
        i_m = i_model // arr_eps.size
        model = models[i_m*arr_eps.size + i_eps]
        ind_y_start = i_m*disp_y
        ind_y_end = (i_m+1)*disp_y
        ind_x_start = i_eps*disp_x
        ind_x_end = (i_eps+1)*disp_x
        compute_start_ind = batch_ind*frames
        for i in range(frames):
            model.run(iters_per_frame)
            grid = model.get_grid()
            arr[i, ind_y_start:ind_y_end, ind_x_start:ind_x_end] = grid
            arr_mean[i_m, i_eps, compute_start_ind+i] = grid.mean()
            arr_std[i_m, i_eps, compute_start_ind+i] = grid.std()


def plot_std(arr_std: np.ndarray, arr_m: np.ndarray, arr_eps: np.ndarray, frames: int, iters_per_frame: int, fig_name: str):
    # fig: plt.Figure = plt.figure(figsize=utils.DISPLAY_SIZE)
    # fig.suptitle("Standard deviation")
    fig: plt.Figure = plt.figure(figsize=(utils.REPORT_SIZE[0], utils.REPORT_SIZE[1]-1))
    ax1: plt.Axes = fig.add_subplot(111)
    iter_axis = np.arange(frames)*iters_per_frame
    for i_m, m in enumerate(arr_m):
        for i_eps, eps in enumerate(arr_eps):
            # ax1.plot(iter_axis, arr_mean[i_m, i_eps, :], label=f"m: {m}, eps: {eps}")
            ax1.plot(iter_axis, arr_std[i_m, i_eps, :], label=f"m: {m}, eps: {eps}")
    ax1.set_ylabel("Standard deviation")
    for ax in (ax1, ):
        ax.set_xlabel("Iterations")
        ax.legend()
    utils.savefig(fig, "./fig/b_std" + fig_name)


def split_arr(arr: np.ndarray, num_m: int, num_eps: int) -> np.ndarray:
    size_y = arr.shape[-2] // num_m
    size_x = arr.shape[-1] // num_eps
    new_arr = np.empty((*arr.shape[:-2], num_m, num_eps, size_y, size_x))
    # It might be possible to do this with np.reshape
    for i_m in range(num_m):
        for i_eps in range(num_eps):
            new_arr[..., i_m, i_eps, :, :] = arr[..., i_m*size_y:(i_m+1)*size_y, i_eps*size_x:(i_eps+1)*size_x]
    return new_arr


def plot_histograms(arr: np.ndarray, arr_m: np.ndarray, arr_eps: np.ndarray, fig_name: str):
    # fig: plt.Figure = plt.figure(figsize=utils.DISPLAY_SIZE)
    # fig.suptitle("Final opinion distribution")
    fig: plt.Figure = plt.figure(figsize=(11, 8))
    for i_m, m in enumerate(arr_m):
        for i_eps, eps in enumerate(arr_eps):
            ax: plt.Axes = fig.add_subplot(arr_m.size, arr_eps.size, i_m*arr_eps.size + i_eps + 1)
            # hist, bin_edges = np.histogram(arr[i_m, i_eps, ...], bins=256, range=[0, 255])
            # ax.hist(hist, bin_edges)
            ax.hist(arr[i_m, i_eps, ...].flatten(), bins=32, range=[0, 256])
            ax.set_title(f"m = {m}, ε = {eps}")
            # ax.set_xlabel("opinion")
            # ax.set_ylabel("count")
    utils.savefig(fig, "./fig/b_histograms" + fig_name)


def plot_frame(arr: np.ndarray, arr_m: np.ndarray, arr_eps: np.ndarray, fig_name: str):
    # fig: plt.Figure = plt.figure(figsize=utils.DISPLAY_SIZE)
    # fig.suptitle("Final state")
    fig: plt.Figure = plt.figure(figsize=utils.REPORT_SIZE)
    ax: plt.Axes = fig.add_subplot()
    pos = ax.imshow(arr, cmap="inferno")
    ax.set_xticks(arr.shape[-1] // arr_eps.size * (0.5 + np.arange(0, arr_eps.size)))
    ax.set_xticklabels(arr_eps)
    ax.set_yticks(arr.shape[-2] // arr_m.size * (0.5 + np.arange(0, arr_m.size)))
    ax.set_yticklabels(arr_m)
    ax.set_xlabel("ε")
    ax.set_ylabel("m")
    fig.colorbar(pos, ax=ax)
    utils.savefig(fig, "./fig/b_frame" + fig_name)


def plot_data(arr: np.ndarray, arr_m: np.ndarray, arr_eps: np.ndarray, fig_name: str):
    # fig: plt.Figure = plt.figure(figsize=utils.DISPLAY_SIZE)
    # fig.suptitle("Final state")
    fig: plt.Figure = plt.figure(figsize=utils.REPORT_SIZE)
    norm = matplotlib.colors.Normalize(vmin=np.min(arr), vmax=np.max(arr))
    imgs = []
    axs = []
    for i_m, m in enumerate(arr_m):
        for i_eps, eps in enumerate(arr_eps):
            ax: plt.Axes = fig.add_subplot(arr_m.size, arr_eps.size, i_m*arr_eps.size + i_eps + 1)
            axs.append(ax)
            img = ax.imshow(arr[i_m, i_eps, ...], cmap="inferno")
            img.set_norm(norm)
            imgs.append(img)
            ax.set_title(f"m = {m}, ε = {eps}")
    fig.colorbar(imgs[0], ax=axs)
    utils.savefig(fig, "./fig/b_data" + fig_name)


def part_b():
    arr_eps = np.array([10, 50, 100], dtype=np.int_)
    arr_m = np.array([0.3, 0.6], dtype=np.float_)

    # Official resolution
    # fig_name = "_official"
    fig_name = "_report_official"
    disp_x = 50
    disp_y = 50
    frames = 5000
    iters_per_frame = 100
    frames_per_batch = 5000

    # Full HD
    # fig_name = "_highres"
    # disp_x = 640
    # disp_y = 540
    # frames = 10000
    # iters_per_frame = 10000
    # frames_per_batch = 1000

    # Video flags
    make_video = True
    convert_video = True

    n = disp_x * disp_y
    batches = frames // frames_per_batch
    print("Total resolution:", (arr_eps.size * disp_x, arr_m.size * disp_y))
    print(f"Length of video: {frames // 3600} min {frames % 3600 // 60} s")

    arr_mean = np.empty((arr_m.size, arr_eps.size, frames))
    arr_std = np.empty((arr_m.size, arr_eps.size, frames))

    models = numba.typed.List()
    for i_m, m in enumerate(arr_m):
        for i_eps, eps in enumerate(arr_eps):
            models.append(relative_model.RelativeModel(n, eps, m, disp_x, disp_y))

    if make_video:
        vid = video.Video(
            (arr_m.size * disp_y, arr_eps.size * disp_x),
            path=f"./video/b{fig_name}.mp4",
            temp_path=f"./video/b{fig_name}_temp.mp4")
    else:
        vid = None
    arr = np.empty((frames_per_batch, arr_m.size*disp_y, arr_eps.size*disp_x), dtype=np.uint8)
    for i_batch in range(batches):
        start_time = time.perf_counter()
        part_b_core(arr, models, arr_eps, arr_m, arr_mean, arr_std, disp_x, disp_y, frames_per_batch, iters_per_frame, i_batch)
        compute_ready = time.perf_counter()
        if make_video:
            for i_frame in range(frames_per_batch):
                vid.frame(arr[i_frame, :, :])
        print(
            f"Batch: {i_batch}, processed: {(i_batch+1)*frames_per_batch*100 / frames:.1f} %, "
            f"compute: {compute_ready - start_time :.2f}, video: {time.perf_counter() - compute_ready}")

    final_frame = arr[-1, :, :].copy()
    final_data = split_arr(final_frame, arr_m.size, arr_eps.size)

    # Free memory for video conversion
    del arr
    del models
    gc.collect()

    # Video processing
    if convert_video:
        thr = threading.Thread(target=vid.process)
        thr.start()
    else:
        thr = None

    # Plotting
    plot_std(arr_std, arr_m, arr_eps, frames, iters_per_frame, fig_name)
    plot_histograms(final_data, arr_m, arr_eps, fig_name)
    plot_frame(final_frame, arr_m, arr_eps, fig_name)
    plot_data(final_data, arr_m, arr_eps, fig_name)

    plt.show()

    if convert_video:
        print("Waiting for video conversion to finish")
        thr.join()
        print("Ready")


if __name__ == "__main__":
    part_b()
