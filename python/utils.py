import typing as tp

import matplotlib.pyplot as plt
import numba
import numpy as np

DISPLAY_SIZE = (1920/100, 1080/100)
REPORT_SIZE = (11, 6)
REPORT_FULL_PAGE = (9.4, 14)
A3_SIZE = (16.53, 11.69)
A4_SIZE = (11.69, 8.27)


@numba.generated_jit(nopython=True)
def randint(low: int, high: int, shape: tp.Union[int, tp.Tuple[int, ...]], dtype=np.int_):
    if isinstance(shape, (numba.types.Integer, numba.types.IntegerLiteral)):
        def func(low, high, shape, dtype=np.int_):
            arr = np.empty(shape, dtype=dtype)
            for i in range(shape):
                arr[i] = np.random.randint(low, high)
            return arr
        return func
    elif isinstance(shape, (numba.types.UniTuple, numba.types.Array)):
        def func(low, high, shape, dtype=np.int_):
            arr = np.empty(shape, dtype=dtype)
            for index, _ in np.ndenumerate(arr):
                arr[index] = np.random.randint(low, high)
            return arr
        return func
    else:
        raise numba.TypingError("shape has to be int or tuple")


@numba.njit
def random_arr(shape: tp.Tuple[int, ...]) -> np.ndarray:
    arr = np.empty(shape)
    for index, _ in np.ndenumerate(arr):
        arr[index] = np.random.rand()
    return arr


def savefig(
        fig: plt.Figure, path: str,
        orientation: str = "landscape",
        bbox_inches: str = "tight",
        papertype: tp.Optional[str] = None):
    extensions = [
        ".eps",
        ".pdf",
        ".png",
        ".svg"
    ]
    for extension in extensions:
        fig.savefig(
            path + extension,
            papertype=papertype,
            orientation=orientation, bbox_inches=bbox_inches)
