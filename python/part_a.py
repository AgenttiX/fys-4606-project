import matplotlib
import matplotlib.pyplot as plt
import numba
import numpy as np
import scipy.optimize

import utils
import voter_model

matplotlib.use("TkAgg")


@numba.njit(parallel=True)
def part_a_core(n_rho0: int, n_runs: int):
    arr_rho0 = np.linspace(0, 1, n_rho0)
    arr_steps = np.empty((n_rho0, n_runs))
    arr_cons = np.empty((n_rho0, n_runs))

    for i_rho in numba.prange(n_rho0):
        print("Processing i_rho:", i_rho)
        for i_run in numba.prange(n_runs):
            model = voter_model.VoterModel2D((10, 10), arr_rho0[i_rho])
            steps = 0
            while not model.has_consensus():
                model.copy_from_neighbor()
                steps += 1
            arr_steps[i_rho, i_run] = steps
            arr_cons[i_rho, i_run] = model.has_consensus()
    return arr_rho0, arr_steps, arr_cons


def part_a1():
    """Varying rho0"""
    n_rho0 = 50
    n_runs = 200
    arr_rho0, arr_steps, arr_cons = part_a_core(n_rho0, n_runs)

    arr_steps_mean = arr_steps.mean(1)
    prob_cons1 = ((arr_cons + 1) // 2).mean(1)
    steps_coeff = np.polyfit(arr_rho0, arr_steps_mean, 2)
    prob_cons1_coeff = np.polyfit(arr_rho0, prob_cons1, 1)
    steps_fit = np.polyval(steps_coeff, arr_rho0)
    prob_cons1_fit = np.polyval(prob_cons1_coeff, arr_rho0)

    # fig: plt.Figure = plt.figure(figsize=utils.DISPLAY_SIZE)
    fig: plt.Figure = plt.figure(figsize=utils.REPORT_SIZE)
    # fig.suptitle(r"Varying the initial density of +1 ($\rho_0$)")
    ax1 = fig.add_subplot(121)
    ax1.plot(arr_rho0, arr_steps_mean, label="data")
    ax1.plot(arr_rho0, steps_fit, label=f"fit: {steps_coeff[0]:.2f}x²{steps_coeff[1]:+.2f}x{steps_coeff[2]:+.2f}")
    ax1.set_xlabel(r"$\rho_0$")
    ax1.set_ylabel("n_steps for consensus")
    ax1.legend()

    ax2 = fig.add_subplot(122)
    ax2.plot(arr_rho0, prob_cons1, label="data")
    ax2.plot(arr_rho0, prob_cons1_fit, label=f"fit: {prob_cons1_coeff[0]:.3f}x{prob_cons1_coeff[1]:+.3f}")
    ax2.set_xlabel(r"$\rho_0$")
    ax2.set_ylabel("Probability of consensus = +1")
    ax2.legend()

    # utils.savefig(fig, "./fig/a1")
    utils.savefig(fig, "./fig/a1_report")


@numba.njit
def consensus_steps(model: voter_model.VoterModel):
    steps = 0
    while not model.has_consensus():
        model.copy_from_neighbor()
        steps += 1
    return steps


@numba.njit(parallel=True, cache=True)
def part_a2_core(sizes: np.ndarray, n_runs: int, rho0: float):
    arr_steps = np.zeros((3, sizes.size, n_runs))
    for i_size in range(sizes.size):
        size = sizes[i_size]
        print("Running for size", size)
        for i_run in numba.prange(n_runs):
            model1 = voter_model.VoterModel1D((size,), rho0)
            arr_steps[0, i_size, i_run] = consensus_steps(model1)
            model2 = voter_model.VoterModel2D((size, size), rho0)
            arr_steps[1, i_size, i_run] = consensus_steps(model2)
            model3 = voter_model.VoterModel3D((size, size, size), rho0)
            arr_steps[2, i_size, i_run] = consensus_steps(model3)
    return arr_steps


def fit_n_log_n(x, a):
    return a*x*np.log(x)


def fit_exp(x, a, b):
    return a*np.exp(b*x)


def fit_square(x, a):
    return a*x**2


def fit_cubic(x, a):
    return a*x**3


def fit_custom_power(x, a, b):
    return a*x**b


def part_a2():
    """Varying the lattice size"""
    # For testing
    # sizes = np.arange(1, 11)
    # For good graphs
    sizes = np.arange(3, 14)

    arr_steps = part_a2_core(sizes, n_runs=32, rho0=0.6)
    arr_steps_mean = arr_steps.mean(2)
    arr_steps_range = np.stack((np.min(arr_steps, axis=2), np.max(arr_steps, axis=2)))
    # arr_steps_std = arr_steps.std(2)

    arr_N = np.tile(sizes, (3, 1))

    # fig: plt.Figure = plt.figure(figsize=utils.DISPLAY_SIZE)
    # axes: np.ndarray = fig.subplots(2, 3)
    fig: plt.Figure = plt.figure(figsize=utils.REPORT_FULL_PAGE)
    axes: np.ndarray = fig.subplots(3, 2)
    axes = axes.transpose()

    for i in range(3):
        arr_N[i, :] = arr_N[i, :]**(i+1)

        fitd = scipy.optimize.curve_fit(fit_n_log_n, arr_N[i, :], arr_steps_mean[i, :])
        fitd_square = scipy.optimize.curve_fit(fit_square, arr_N[i, :], arr_steps_mean[i, :])
        fitd_cubic = scipy.optimize.curve_fit(fit_cubic, arr_N[i, :], arr_steps_mean[i, :])
        fitd_custom = scipy.optimize.curve_fit(fit_custom_power, arr_N[i, :], arr_steps_mean[i, :])
        fitd_exp = scipy.optimize.curve_fit(fit_exp, arr_N[i, :], arr_steps_mean[i, :]) if i == 0 else None

        ax_lin: plt.Axes = axes[0, i]
        ax_log: plt.Axes = axes[1, i]

        ax_lin.plot(sizes, arr_steps_mean[i, :], label="data", marker="*", linestyle="")
        ax_log.errorbar(sizes, arr_steps_mean[i, :], yerr=arr_steps_range[:, i, :], label="data", marker="*", linestyle="")
        for ax in (ax_lin, ax_log):
            ax.plot(sizes, fit_n_log_n(arr_N[i, :], *fitd[0]), label="N log N")
            ax.plot(sizes, fit_square(arr_N[i, :], *fitd_square[0]), label="$N^2$")
            ax.plot(sizes, fit_cubic(arr_N[i, :], *fitd_cubic[0]), label="$N^3$")
            ax.plot(sizes, fit_custom_power(arr_N[i, :], *fitd_custom[0]), label="$N^{" + f"{fitd_custom[0][1]:.2f}" + "}$")
            if i == 0:
                ax.plot(sizes, fit_exp(arr_N[i, :], *fitd_exp[0]), label="exp N")

        ax_log.set_yscale("log")
        ax_lin.set_title(f"{i+1}D, linear y axis")
        ax_log.set_title(f"{i+1}D, logarithmic y axis")

    # fig.suptitle("Iterations required for consensus")
    for i, ax in np.ndenumerate(axes):
        ax.set_ylabel("mean iterations for consensus")
        ax.set_xlabel("lattice size")
        ax.legend()

    # utils.savefig(fig, "./fig/a2", papertype="a3")
    utils.savefig(fig, "./fig/a2_report")


def main():
    part_a1()
    # part_a2()
    plt.show()


if __name__ == "__main__":
    main()
