import typing as tp

import numba
import numpy as np

import utils


@numba.jitclass([
    ("L", numba.int_),
    ("fractions", numba.double[:]),
    ("lattice", numba.uint8[:, :])
])
class SznajdModel:
    def __init__(self, L: int, fractions: np.ndarray):
        if round(fractions.sum(), 2) != 1:
            raise ValueError("Percentages have to sum to one")

        self.L = L
        self.fractions = fractions

        # If all probabilities are equal
        if np.all(fractions == fractions[0]):
            self.lattice = utils.randint(0, self.fractions.size, (L, L), dtype=np.uint8)
        else:
            self.lattice = np.zeros((self.L, self.L), dtype=np.uint8)
            for i_p in range(1, self.fractions.size):
                p = self.fractions[i_p]
                target = round(p * self.L ** 2)
                n = 0
                while n < target:
                    i = np.random.randint(self.L)
                    j = np.random.randint(self.L)
                    if not self.lattice[i, j]:
                        self.lattice[i, j] = i_p
                        n += 1

    def set_neighbors(self, i: int, j: int):
        val = self.lattice[i, j]
        self.lattice[(i + 1) % self.L, j] = val
        self.lattice[(i - 1) % self.L, j] = val
        self.lattice[i, (j + 1) % self.L] = val
        self.lattice[i, (j - 1) % self.L] = val

    def run(self, iters: int = 1):
        for _ in range(iters):
            i = np.random.randint(self.L)
            j = np.random.randint(self.L)
            neigh_ind = np.random.randint(4)
            if neigh_ind == 0:
                n_i = (i+1) % self.L
                n_j = j
            elif neigh_ind == 1:
                n_i = (i-1) % self.L
                n_j = j
            elif neigh_ind == 2:
                n_i = i
                n_j = (j+1) % self.L
            else:
                n_i = i
                n_j = (j-1) % self.L
            if self.lattice[i, j] == self.lattice[n_i, n_j]:
                self.set_neighbors(i, j)
                self.set_neighbors(n_i, n_j)

    # This is twice as slow as using np.all()
    # def converged(self) -> bool:
    #     first = self.lattice[0, 0]
    #     for i in range(self.L):
    #         for j in range(self.L):
    #             if self.lattice[i, j] != first:
    #                 return False
    #     return True

    def run_until_converged(self, iters_per_check: int, max_checks: int) -> tp.Tuple[int, int]:
        for i_check in range(max_checks):
            self.run(iters_per_check)

            # This is slower than the method below
            # lattice_min = self.lattice.min()
            # if self.lattice.max() == lattice_min:

            if np.all(self.lattice == self.lattice[0, 0]):
                return i_check*iters_per_check, self.lattice[0, 0]
        return -1, -1
