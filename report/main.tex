\documentclass{scrartcl}
\usepackage[utf8]{inputenc}

\usepackage[backend=biber, sorting=none]{biblatex}
\addbibresource{references.bib}

\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{placeins}	% for \FloatBarrier
\usepackage{subcaption}
% \usepackage{xcolor}

\title{Models of opinion formation}
\subtitle{FYS-4606 Statistical physics 2 project work}
\author{Mika Mäki}
\date{April 2020}

\begin{document}

\maketitle
\tableofcontents

\section{Introduction}
Social dynamics and opinion formation are as complex processes as humans themselves.
However, their basic aspects can be captured by relatively simple models based on statistical physics.
In this work three models, the voter model, the relative agreement interaction model, and the Sznajd model are simulated and analyzed.
All three models are variations of the Ising model, which is the foundation for a significant fraction of statistical physics.
The models have a rectangular lattice of individuals, which interact iteratively as randomly chosen pairs.


\clearpage
\section{Voter model}
The voter model is the simplest of the models discussed in this work.
Each individual is assigned a binary opinion ($\pm 1$) and a position in a rectangular lattice as in figure \ref{fig:ising}.

\begin{figure}[ht!]
\centering
\includegraphics[width=0.4\textwidth]{../python/fig/ising.pdf}
\caption{The 2D voter model (or the Ising model) with lattice size $L=5$}
\label{fig:ising}
\end{figure}

\FloatBarrier
At each iteration a random individual is chosen and the opinion of their random neighbor is copied to them.
As pseudocode this update rule is below.
Consensus is defined as all individuals having the same opinion.
The starting distribution was varied, and the number of steps required for consensus and the probability of +1 consensus were plotted as a function of the starting fraction of +1 opinions, $\rho_0$, in figure \ref{fig:voter_dist}.
The results are obtained with the lattice size $L=10$ and averaged over 200 runs.

\begin{lstlisting}[language=Python]
while not has_consensus(lattice):
    inds = random_indices()
    inds_neighbor = random_neighbor(inds)
    lattice[inds] = lattice[inds_neighbor]
\end{lstlisting}

It can be seen in figure \ref{fig:voter_dist} that the number of iterations required for consensus is approximately parabolical as a function of +1 opinions in the beginning, $\rho_0$.
The largest number of iterations is required when the starting distribution is even, which is rather logical as such a situation is the furthest away from consensus.
The probability of reaching a consensus of +1 scaled linearly with respect to $\rho_0$.
Despite the simple linear scaling this result is notable, as it shows that the model can converge to the minority opinion even when the minority is very small.
Fundamentally this behaviour is caused by the randomness in the selection of the pairs.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{../python/fig/a1_report.pdf}
\caption{Number of steps required for consensus and probability of the consensus of +1 as functions of the fraction of +1 opinions in the start, $\rho_0$}
\label{fig:voter_dist}
\end{figure}

\FloatBarrier
The dimensionality and size of the lattice have profound effects on the convergence time.
Figure \ref{fig:voter_lattice} illustrates the iterations required for consensus as a function of the lattice size in one-, two- and three-dimensional models.
It should be noted that the number of voters is the lattice size to the power of the dimensionality of the system.
The results were obtained with $\rho_0 = 0.6$ and averaged over 32 runs.
The error bars in the logarithmic plots represent the range values obtained from the runs.
It can be seen that there is nearly an order of magnitude of variation in the number of iterations for each lattice size.
This is caused by scenarios where the system gets stuck in a state where the opinion distribution is close to even, and as a result getting a significant spontaneous fluctuation away from this state takes quite a few iterations.
However, the most significant finding of these plots is that the scaling of the convergence time depends on the dimensionality.
In an one-dimensional system the exponent of the power law is nearly three, in a two-dimensional system it is somewhat below two, and in a three-dimensional system it is close to 1.5.
These findings contrast the expected scaling of $N \log N$ for a 2D system as given in the assignment description.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{../python/fig/a2_report.pdf}
\caption{Iterations required for consensus as a function of lattice size with different dimensionalities}
\label{fig:voter_lattice}
\end{figure}


\clearpage
\section{Relative agreement interaction model}
The relative agreement interaction model is based on the finding that people tend to adopt opinions only from those with whom they already somewhat agree.
To model this, the binary opinion system of the voter model is replaced by a continuum of opinions, or a discrete approximation of it.
In this work the continuum is approximated by a discrete scale of 256 steps, and this is numerically realised by a lattice of eight-bit unsigned integers (uint8).
The model has two control parameters. $\epsilon$ denotes the maximum opinion difference that two individuals can have to absorb opinions from one another, and $m$ denotes the total relative change in their opinions when they interact.
The model proceeds iteratively according to the following rules.

\begin{lstlisting}[language=Python]
for i_iter in range(iterations):
    i = random_index()
    j = random_index()
    o_i = lattice[i]
    o_j = lattice[j]
    if abs(o_i - o_j) < epsilon:
        lattice[i] -= m/2 * (o_i - o_j)
        lattice[j] += m/2 * (o_i - o_j)
\end{lstlisting}

In this project the model was evaluated with six different combinations of the control parameters. Videos of the time evolution of the model are available at the GitLab repository. \cite{repo}
Figure \ref{fig:relative_frame} shows a final frame of such a video.
The frame consists of six individual runs of the simulation with different control parameters displayed side-by-side.
It should be noted that the arrangement of the pixels is random, as the interacting pair is chosen randomly for each iteration, and this selection does not depend on the distance between the individuals.
Figure \ref{fig:relative_data} shows the same data as figure \ref{fig:relative_frame} but the different runs are separated with the control parameters displayed next to them.
The lattice size $L = 50$ and the simulation was run for 500 000 iterations.
It can be seen that the runs with the largest control parameters have equilibrated close to a consensus, but smaller control parameters have resulted in more disagreement among the individuals.

\begin{figure}[ht!]
\centering
\includegraphics[width=0.9\textwidth]{../python/fig/b_frame_report_official.pdf}
\caption{Final frame of a time evolution video of the relative agreement interaction model with six different combinations of control parameters}
\label{fig:relative_frame}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=0.9\textwidth]{../python/fig/b_data_report_official.pdf}
\caption{Final states of a time evolution simulation of the relative agreement interaction model with six different combinations of control parameters}
\label{fig:relative_data}
\end{figure}

The details of the time evolution of the model cannot be properly illustrated in a static document, and therefore the reader is advised to check the GitLab repository for the videos. \cite{repo}
However, figure \ref{fig:relative_std} illustrates the coalescing of opinions as a function of the number of iterations by utilizing the standard deviation of the opinions.
In the runs where one or a few shared opinions emerge, the overall distribution of opinions narrows significantly, and this seen in the standard deviation.
It can be seen that increasing either $m$ or $\epsilon$ or both decreases the convergence time.
The effect of increasing $m$ is explained by the fact that the higher the parameter is, the more the opinions of the chosen pair are shifted towards each other during a single iteration.
However, the value of $\epsilon$ appears to have a significant effect on the equilibrium value.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{../python/fig/b_std_report_official.pdf}
\caption{Standard deviation of the opinion distribution as a function of the iterations}
\label{fig:relative_std}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{../python/fig/b_histograms_report_official.pdf}
\caption{Standard deviation of opinion as a function of the number of iterations in the relative agreement interaction model}
\label{fig:relative_histograms}
\end{figure}

\FloatBarrier
The differences caused by the control parameters and illustrated by the standard deviation become more apparent when the final opinions are viewed as a histogram as in figure \ref{fig:relative_histograms}.
The higher the value of $\epsilon$ is, the larger is the area from which a positive fluctuation in the opinion distribution will start to gather supporters.
If $\epsilon$ is large enough, nearly all of the individuals will reach consensus, and this has happened with $\epsilon = 100$ regardless of the value of $m$.
However, there were several individuals that held to their extreme views, as they did not interact with individuals with more modest opinions before the majority drifted so far apart from them that they could not interact.
To the best knowledge of the author this phenomenon is also present in human societies.
On the other hand, if $\epsilon$ is very small, the initially even distribution will split into peaks of various opinions, the separation of which will be larger than $\epsilon$.
If $m$ is small, this state will take longer to form, as individuals with opinions between them will not drift towards the peaks as quickly.
This will result in some individuals retaining opinions between the peaks until they can no longer interact.
This behaviour is also seen with both values of $m$ for $\epsilon = 50$, as there is a very small peak between the two primary peaks in the distribution.

The model was also tested with a larger lattices such as 640x540 to create high-resolution videos (1920x1080).
It was found that the basic features stayed the same, but the number of iterations required and therefore computational complexity increased drastically, and the visualizations weren't as illustrative due to the small pixel size.
However, with $m = 0.3$ and $\epsilon = 10$ the resulting distribution was much more even than with $L = 50$.
This is can be explained by the number of individuals, since for a larger lattice it is much more likely that there are individuals with opinions between two peaks, and therefore separating the peaks would require a lot more iterations.
Based on analyses of the standard deviation this time scaling seems to be higher than for the initial change of the distribution.


\clearpage
\section{Sznajd model}
The Sznajd model focuses on the effects of peer pressure, also known as social validation.
It was first presented by Katarzyna Sznajd-Weron and J\'{o}zef Sznajd in their paper \textit{Opinion evolution in closed community} in the year 2000.
The original model is based on a one-dimensional lattice of individuals with two possible opinions.
At each iteration a pair of neighboring individuals is chosen at random, and the states of their neighbors are updated according to two rules.
If the chosen pair shares the same opinion, their neighbors will start to agree with them.
This is known as social validation.
However, if the chosen pair disagrees, their neigbors will adopt opposite opinions. This is referred to by the statement \textit{discord destroys}.
Over time this model will reach one of two possible steady states.
One of these is complete consensus, and this is known as the ferromagnetic state in analogy with the Ising model.
The other possibility is complete disagreement, in which all neighbors disagree with each other.
With a similar analogy this is known as the antiferromagnetic state.
\cite{sznajd}

In this work the one-dimensional lattice has been replaced by a two-dimensional one.
As a result, each randomly chosen pair has six neighbors instead of two, since each individual has four neighbors, but two of these overlap with either the other cell or its neighbors for any given pair.
Pseudocode for this model is given below.

\begin{lstlisting}[language=Python]
for i in range(iterations):
    inds = random_index()
    inds_neighbor = random_neighbor(i, j)
    if lattice[inds] == lattice[inds_neighbor]:
        set_neighbors_to_same(inds)
        set_neighbors_to_same(inds_neighbor)
\end{lstlisting}

This model results in a rapid coarsening from the initial random distribution as illustrated in figure \ref{fig:sznajd_evolution}.
In the figure the columns denote different initial distributions of opinions and the rows denote iteration counts.

It was found that this model always resulted in a consensus.
However, reaching consensus could take a very long time, especially if the domain walls become almost straight at some point during the time evolution.
An example of this would be a straight stripe of one opinion in a sea of another.
As long as such a situation persists, the transition probabilities become irrelevant of the overall distribution of opinions, and reaching consensus will take a lot of iterations, since the breaking of this state will have to start from a random fluctuation which then grows to fill the system.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{../python/fig/c_evolution_report.pdf}
\caption{Time evolution of the Sznajd model}
\label{fig:sznajd_evolution}
\end{figure}

Figures \ref{fig:sznajd_mean}, \ref{fig:sznajd_mean_nonconverged} and \ref{fig:sznajd_mean_minority} show the mean opinions over time in different simulation runs.
It can be seen that the further the initial mean opinion is from $0.5$, the faster the convergence is and the more it resembles an exponential curve.
However, if the initial distribution is nearly even, randomness has a significant role in the time evolution of the system, and even converging to the minority is possible.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{../python/fig/c_mean_report.pdf}
\caption{Mean opinion of the Sznajd model over time}
\label{fig:sznajd_mean}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{../python/fig/c_mean_report_nonconverged.pdf}
\caption{Mean opinion of the Sznajd model over time in a case when one of the runs did not converge in time}
\label{fig:sznajd_mean_nonconverged}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{../python/fig/c_mean_report_conv_to_one.pdf}
\caption{Mean opinion of the Sznajd model over time in a case when one of the runs converged to the minority}
\label{fig:sznajd_mean_minority}
\end{figure}


\FloatBarrier
\subsection{Generalizing the Sznajd model for multiple opinions}

As the final part of this work the Sznajd model was generalized to account for more than two opinions.
Simulations were run an even initial distribution with lattice size $L = 50$ and averaged over 64 runs.
The results are in figure \ref{fig:sznajd_multi}, in which the error bars denote the standard deviation.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{../python/fig/d_report.pdf}
\caption{Iterations required for convergence and average consensus opinion for the Sznajd model with multiple opinions}
\label{fig:sznajd_multi}
\end{figure}

It can be seen that consensus is reached regardless of the number of different opinions, but the number of iterations required for consensus increases linearly with respect to the number of opinions.
The figure for the average consensus opinion as a function of the number of different opinions is merely for showing that the chosen lattice size and number of runs are sufficient for modeling the problem.


\clearpage
\section{Summary}
There are several models for opinion formation, and each of them focuses on specific aspects of the problem.
Rather obviously none of the tested models capture the intricacies of sociology, but they serve as important approximations for demonstrating why certain features emerge.
An example of such are the outliers in the relative agreement interaction model, which are found at both extremes and can be recognized as supporters of past ideologies that the vast majority no longer considers as valid options.
Another thing to note is how these outliers are immediately visible in the 2D visualization even though they cannot be seen in the histogram due to their small number.
This could be considered analogous to the media presence of such individuals, as humans tend to spot and remember exotic opinions in a way that is disproportionate to their occurrence in the population.

To improve these models they should be combined and extended.
The Sznajd model is too discrete to account how supporters of similar opinions may have slight disagreements, and the relative agreement interaction model is not usable for modeling voting in a multi-party system.
However, these models could be combined for example by modeling opinions as multidimensional vectors instead of scalars, choosing the pairs as in the Sznajd model, determining the suitable relative agreement of the pair by the difference of their vectors, and summing either the average or the difference vector to the neighbors depending on the magnitude of the difference in the opinions of the pair.
However, the computational complexity of such a model would be significantly higher than in the models tested so far, and the tested models already provided interesting features to analyze and interpret.

Special thanks are given to \href{https://numba.pydata.org/}{Numba} for enabling rapid prototyping of the simulations with high performance, and to \href{https://opencv.org/}{OpenCV} for enabling the creation of time evolution videos with high resolution and large frame counts.

The source code of the simulations is available at the GitLab repository. \cite{repo}
As an afterthought the code should have been organized so that the simulation results are saved to disk and then read by another script that plots the data.
This would have enabled the use of much longer simulation runs while retaining the flexibility in adjusting the plotting.
It would have also enabled the creation of videos with higher resolution, but these would have been mostly cosmetic improvements, as the details of the behaviour of the models are already captured with the existing simulations.


\clearpage
\printbibliography[heading=bibintoc]

\end{document}
